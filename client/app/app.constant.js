(function(angular, undefined) {
'use strict';

angular.module('dcmnChallengeApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);