'use strict';

(function() {

class MainController {
  constructor() {
    this.widgets = [];
    this.newWidgetText = "";
    this.rows = [];
    this.addWidget = function() {
      this.widgets.push({ text: this.newWidgetText });
      this.newWidgetText = "";
      this.rows = chunk(this.widgets, 3);
    }

    this.removeWidget = function(index){
      console.log(index);
      this.widgets.splice(index, 1);
      this.rows = chunk(this.widgets, 3);
    }

    function chunk(arr, size) {
      var newArr = [];
      for (var i=0; i<arr.length; i+=size) {
        newArr.push(arr.slice(i, i+size));
      }
      return newArr;
    }

  }



}

angular.module('dcmnChallengeApp')
  .controller('MainController', MainController);

})();
